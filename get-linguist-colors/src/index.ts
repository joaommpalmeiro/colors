import clipboard from 'clipboardy'
import fs from 'node:fs'
import path from 'node:path'
import slugify from 'slugify'
import {parse} from 'yaml'

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/throw#another_example_of_throwing_an_object
class NoColorError extends Error {
  constructor(lang: string) {
    super(`${lang} doesn't have a defined color.`)
  }
}

const LANGS = [
  'Python',
  'Jupyter Notebook',
  // 'LTspice Symbol'
]

const linguistFile = fs.readFileSync(path.join(__dirname, 'linguist.yml'), 'utf8')

// https://eemeli.org/yaml/#yaml-parse
const parsedLinguistFile = parse(linguistFile, {strict: true})

// console.log(typeof parsedLinguistFile)
// console.log(parsedLinguistFile)
// console.log(parsedLinguistFile['Python'])
// console.log(parsedLinguistFile['Jupyter Notebook'])

// https://github.com/you-dont-need/You-Dont-Need-Lodash-Underscore#_pick
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map#using_map_to_reformat_objects_in_an_array
const output = LANGS.map(lang => {
  const langColor = parsedLinguistFile[lang].color

  // https://dmitripavlutin.com/check-if-object-has-property-javascript/#3-comparing-with-undefined
  if (langColor === undefined) {
    throw new NoColorError(lang)
  }

  // https://www.npmjs.com/package/slugify
  return {language: lang, slug: slugify(lang, {lower: true}), color: langColor.toLowerCase()}
})
console.log(output)

// https://github.com/sindresorhus/clipboardy/releases
// https://github.com/sindresorhus/clipboardy#api
clipboard.writeSync(JSON.stringify(output))
console.log('Copied to clipboard!')
