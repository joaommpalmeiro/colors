// https://nodejs.dev/en/api/v18/https/
// https://sebhastian.com/nodejs-download-file/
// https://github.com/kevva/download

import fs from 'node:fs'
import https from 'node:https'
import path from 'node:path'

const FILE_URL = 'https://raw.githubusercontent.com/github/linguist/v7.24.1/lib/linguist/languages.yml'

https.get(FILE_URL, res => {
  const outputPath = path.join(__dirname, 'linguist.yml')
  const writeStream = fs.createWriteStream(outputPath)

  res.pipe(writeStream)

  writeStream.on('finish', () => {
    writeStream.close()
    console.log('Download completed!')
  })
})
