# get-linguist-colors

## References

- https://github.com/github/linguist
- https://github.com/github/linguist/blob/v7.24.1/lib/linguist/languages.yml
- https://sebhastian.com/nodejs-download-file/

## Development

```bash
nvm install && nvm use && node --version
```

```bash
npm install
```

```bash
npm run dev:download
```

```bash
npm run dev
```

## Notes

- https://www.npmjs.com/package/yaml
- `npm install yaml slugify clipboardy@2.3.0 && npm install -D typescript ts-node @tsconfig/node18 prettier @github/prettier-config`
- https://www.schemastore.org/json/ + https://github.com/redhat-developer/vscode-yaml
- https://github.com/DBozhinovski/tailwind-reasonable-colors + https://reasonable.work/colors/
